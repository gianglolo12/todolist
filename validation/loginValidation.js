import { check } from 'express-validator/check';
import { transValidation } from '../Lang/vi';

let register = [
    check("reg_email", transValidation.email_incorrect)
    .isEmail()
    .trim(),
    check('reg_reenter_pass', transValidation.confirmPassword_inccorect)
    .custom((value, { req }) => {
        return value === req.body.reg_pass;
    })
];

module.exports = {
    register: register
}