import express from 'express';
import bodyParser from "body-parser";
import morgan from "morgan";
import connectDB from './connectDB';
import mongoose from 'mongoose';
import connectFlash from 'connect-flash';
import passport from 'passport';
//Import todoController
import routTodo from './api/Routes/todoRout';
import routLogin from './api/Routes/loginRout';
import configSession from './config/session';
//function connect DB import from difference Module
connectDB();
let app = express();
let port = process.env.port || 2606;
//---------------------------------------------------//
configSession(app);
app.set("view engine", "ejs");
app.use("/assets", express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(connectFlash());
app.use(passport.initialize());
app.use(passport.session());
app.use(morgan("dev"));
//--------------------------------------------------//
app.use(routTodo);
app.use(routLogin);
//------------------------------------------------ //  
app.listen(port, () => {
    console.log(`"Server running on port ${port}"`);
});