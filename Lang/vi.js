export const transValidation = {
    email_incorrect: "Email phải có dạng xxx@xxx.com nha~~",
    confirmPassword_inccorect: "Sao mài nhập pass không trùng mài???",
}
export const transErrors = {
    account_in_use: 'Đã có một đứa nào đó dùng mail này đăng kí trước mày rồi...nhé! :))',
    login_fail: "Tài Khoản hoặc Mật Khẩu sai nha màiiii !",
    server_error: "Lỗi trên Server rồi mày ơi ! Quay lại sau nhé"
};
export const transSuccess = {
    userCreated: (userEmail) => {
        return `Chúc mừng mày: <b>${userEmail}</b> đã đăng kí thành công! :)`
    },
    loginSuccess: (username) => {
        return `Xin chào ${username}`;
    }
}