import session from "express-session";
import connectMongo from "connect-mongo";
let mongoStore = connectMongo(session);
let DB_CONNECTION = "mongodb";
let DB_HOST = "localhost";
let DB_PORT = 27017;
let DB_NAME = "todolist";
//Variable is where save Session( MonGODB)
let sessionStore = new mongoStore({
    url: `${DB_CONNECTION}://${DB_HOST}:${DB_PORT}/${DB_NAME}`,
    autoReconnect: true,
    // autoRemove:"native"
})
let configSession = (app) => {
    app.use(session({
        key: "express.sid",
        secret: "mySecret",
        store: sessionStore,
        resave: true,
        saveUninitialized: true,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 // 86400.000ms = 1 day
        }
    }))
};
module.exports = configSession;