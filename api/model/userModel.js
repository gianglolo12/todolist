import mongoose from 'mongoose';
import bcrypt from 'bcrypt';
//1-create Schema
let Schema = mongoose.Schema;
let userSchema = new Schema({
    name: String,
    email: String,
    password: String,
    role: { type: String, default: 'user' },
    verifyToken: String,
});
userSchema.statics = {
    createNew(item) {
        return this.create(item);
    },
    findByEmail(email) {
        return this.findOne({ 'email': email }).exec();
    },
    findUserById(id) {
        return this.findById(id).exec();
    }
};
userSchema.methods = {
    comparePassword(password) {
        return bcrypt.compare(password, this.password); // return promise has result  is true or false
    }
}

//2- Create Model
let user = mongoose.model('user', userSchema);

module.exports = user;