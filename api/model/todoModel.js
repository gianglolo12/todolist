import mongoose from 'mongoose';
let Schema = mongoose.Schema;
let todoModel = new Schema({
    task: String,
    isDone: Boolean,
    userID: { type: Schema.Types.ObjectId }
});
let todo = mongoose.model('todo', todoModel);

module.exports = todo;