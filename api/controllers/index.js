import todoControllers from "./todoControllers";
import setupController from "./setupController";
import loginControllers from "./loginControllers"

export const todoC = todoControllers;
export const setupC = setupController;
export const controlC = loginControllers;