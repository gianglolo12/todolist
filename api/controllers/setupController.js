let myApp = (req, res) => {
    res.render("index");
};
let login = (req, res) => {
    res.render('login-register', {
        errors: req.flash("errors"),
        success: req.flash("success")
    });
};
module.exports = {
    login: login,
    myApp: myApp
};