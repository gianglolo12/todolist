import passport from 'passport';
import passportLocal from 'passport-local';
import UserModel from '../../model/userModel'
import { transErrors, transSuccess } from '../../../Lang/vi'
let localStrategy = passportLocal.Strategy;
//Valid user
let initPassportLocal = () => {
    passport.use(new localStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, async(req, email, password, done) => {
        try {
            let user = await UserModel.findByEmail(email);
            if (!user) {
                return done(null, false, req.flash('errors', transErrors.login_fail));
            }
            let checkPassword = await user.comparePassword(password);
            if (!checkPassword) {
                return done(null, false, req.flash('errors', transErrors.login_fail))
            } else { return done(null, user); }
        } catch (error) {
            console.log(error);
            return done(null, false, req.flash('errors', transErrors.server_error))
        }
    }));
    passport.serializeUser((user, done) => {
        done(null, user._id);
    }); //ghi thong tin user vao Session
    passport.deserializeUser((id, done) => {
        UserModel.findUserById(id)
            .then(user => {
                return done(null, user);
            })
            .catch(error => {
                return done(error, null)
            });
    });
}
module.exports = initPassportLocal;