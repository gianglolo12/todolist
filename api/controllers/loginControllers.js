import { validationResult } from "express-validator/check"
import { svReg } from "../../public/js/main/services/index";
import User from '../model/userModel'
import { rejects } from "assert";
import { transErros, transSuccess } from '../../Lang/vi'
import passport from 'passport';
let postlogin = (req, res) => {
    console.log(req.body);
};
let postRegister = async(req, res) => {
    // console.log(validationResult(req));
    // console.log(validationResult(req).isEmpty());
    // console.log('------------------------------')
    // console.log(validationResult(req).mapped());
    //-------------------------------------------------------
    let errorArr = [];
    let successArr = [];
    let users = [];
    let validErrors = validationResult(req);
    if (!validErrors.isEmpty()) {
        let errors = Object.values(validationResult(req).mapped());
        errors.forEach(item => {
            errorArr.push(item.msg);
        });
        req.flash('errors', errorArr);
        return res.redirect('/');
    } else {
        try {
            let createSuccess = await svReg.register(req.body.reg_email, req.body.reg_name, req.body.reg_pass);
            successArr.push(createSuccess);
            req.flash('success', successArr);
            return res.redirect('/');
        } catch (error) {
            errorArr.push(error);
            req.flash('errors', errorArr);
            return res.redirect('/');
        }
    };
};

let checkLogin = (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.redirect('/');
    }
    next();
};
let checkLogout = (req, res, next) => {
    if (req.isAuthenticated()) {
        return res.redirect('/todo-list');
    }
    next();
}
let logout = (req, res) => {
    req.logout();
    res.redirect('/');
};
module.exports = {
    postlogin: postlogin,
    postRegister: postRegister,
    checkLogin: checkLogin,
    checkLogout: checkLogout,
    logout: logout
};