import Todos from '../model/todoModel';
import mongoose from 'mongoose';

let getTodo = (res) => {
    Todos.find((err, todos) => {
        if (err) throw err;
        res.json(todos);
    });
};

let getAlltask = (req, res) => {
    getTodo(res);
};
let findTask = (req, res) => {
    Todos.findById({ _id: req.params.id }, (err, todo) => {
        if (err) throw err;
        res.json(todo);
    });
};
let createTask = (req, res) => {
    let taskTodo = {
        task: req.body.task,
        isDone: req.body.isDone,
    };
    Todos.create({
        task: req.body.task,
        isDone: req.body.isDone,
        userID: req.user._id
    }, (err) => {
        console.log(req.body);
        if (err) throw err
        getTodo(res);
    });
};
let updateTask = (req, res) => {
    if (!req.body._id) {
        return res.status(500).send('ID is Require');
    } else {
        Todos.update({
            _id: req.body._id
        }, {
            task: req.body.task,
            isDone: req.body.isDone
        }, (err, todo) => {
            if (err) throw err
            getTodo(res);
        });
    };
};
let deleteTask = (req, res) => {
    Todos.remove({
        _id: req.params.id,
    }, (err) => {
        if (err) throw err
        getTodo(res);
    });
};

module.exports = {
    getAlltask: getAlltask,
    findTask: findTask,
    createTask: createTask,
    deleteTask: deleteTask,
    updateTask: updateTask
}