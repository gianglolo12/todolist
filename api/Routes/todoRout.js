import express from 'express';
import { todoC, setupC } from '../controllers/index'
var router = express.Router();

router.get('/api/todos', todoC.getAlltask);

//Find Task
router.get('/api/todo/:id', todoC.findTask);

//Create Task
router.post('/api/todo', todoC.createTask);

//Update Task
router.put('/api/todo', todoC.updateTask);

//Delete Task
router.delete('/api/todo/:id', todoC.deleteTask);
module.exports = router;