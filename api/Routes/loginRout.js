import express from 'express';
import { todoC, setupC, controlC } from '../controllers/index';
import { loginValid } from "../../validation/index"
import initPassportLocal from '../../api/controllers/passportControllers/local';
import passport from 'passport';
initPassportLocal();
const router = express.Router();
router.get('/logout', controlC.logout);
router.get('/todo-list', controlC.checkLogin, setupC.myApp);
router.get('/', controlC.checkLogout, setupC.login);
router.post('/register', loginValid.register, controlC.postRegister);
router.post('/login', passport.authenticate('local', {
    successRedirect: '/todo-list',
    failureRedirect: '/',
    successFlash: true,
    failureFlash: true
}));


module.exports = router;