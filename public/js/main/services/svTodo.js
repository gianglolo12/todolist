let app1 = angular.module('app.todos');
app1.factory('svTodo', ['$http', ($http) => {
    return {
        get: () => {
            return $http.get('/api/todos');
        },
        create: (Data) => {
            return $http.post('/api/todo', Data);
        },
        update: (Data) => {
            return $http.put('/api/todo', Data);
        },
        delete: (id) => {
            return $http.delete('/api/todo/' + id);
        }
    }
}]);