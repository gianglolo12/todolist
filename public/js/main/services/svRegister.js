import userModel from "../../../../api/model/userModel";
import bcrypt from "bcrypt";
import uuidv4 from "uuid/v4";
import { rejects } from "assert";
import { transErrors, transSuccess } from "../../../../Lang/vi";
let saltRounds = 2;
let register = (email, name, password) => {
    return new Promise(async(resolve, rejects) => {
        let userByEmail = await userModel.findByEmail(email);
        if (userByEmail) {
            return rejects(transErrors.account_in_use)
        }
        let salt = bcrypt.genSaltSync(saltRounds);
        let userItem = {
            email: email,
            name: email.split('@')[0],
            password: bcrypt.hashSync(password, salt),
            verifyToken: uuidv4()
        };
        let user = await userModel.createNew(userItem);
        resolve(transSuccess.userCreated(user.email));
    });
}
module.exports = {
    register: register
}