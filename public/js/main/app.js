let app = angular.module("app.todos", ["xeditable"]);
app.controller("todoController", ['$scope', 'svTodo', ($scope, svTodo) => {
    $scope.appName = 'To Do List';
    $scope.formData = {};
    $scope.todo = [];
    $scope.loading = true;
    //Load Data from API
    svTodo.get().then((response) => {
        $scope.todo = response.data;
        $scope.loading = false;
    });
    $scope.createTodo = (err) => {
        if (!err) {
            $scope.loading = true;
            var todo = {
                task: $scope.formData.text,
                isDone: false,
            };

            svTodo.create(todo).then((response) => {
                $scope.todo = response.data;
                $scope.formData.text = '';
                $scope.loading = false;
            });
        } else {
            console.log(err);
        }

    };
    $scope.updateTodo = (todo) => {
        $scope.loading = true;
        svTodo.update(todo).then((response) => {
            $scope.todo = response.data;
            $scope.loading = false;
        });
    };

    $scope.deleteTodo = (todo) => {
        $scope.loading = true;
        svTodo.delete(todo._id).then((response) => {
            $scope.todo = response.data;
            $scope.loading = false;
        });
    };
}]);
app.run(['editableOptions', function(editableOptions) {
    editableOptions.theme = 'bs4'; // Can be also 'bs4', 'bs2', 'default'
}]);
// Fix Erro unHandle 
app.config(['$qProvider', function($qProvider) {
    $qProvider.errorOnUnhandledRejections(false);
}]);