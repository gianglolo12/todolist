let mongoose = require('mongoose');
let connectDB = () => {

    let DB_CONNECTION = "mongodb";
    let DB_HOST = "localhost";
    let DB_PORT = 27017;
    let DB_NAME = "todolist";
    let DB_USERNAME = "";
    let DB_PASSWORD = "";

    //--------------------------------------------------------------------
    //mongodb://localhost:27017/todolist
    let URI = `${DB_CONNECTION}://${DB_HOST}:${DB_PORT}/${DB_NAME}`;
    //--------------------------------------------------------------------
    return mongoose.connect(URI, { useMongoClient: true }, (err) => {
        if (err) throw err
        console.log('Connect DB Success!!');
    });
};
module.exports = connectDB;